import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import { WatchQueryOptions, gql } from '@apollo/client/core';
import { stripIgnoredCharacters } from 'graphql';

@Injectable({
  providedIn: 'root'
})
export class GraphQLService {

  constructor(private apollo: Apollo) {   }
  // crear un getData generico 
  getData(query, variables) {
    return this.apollo.watchQuery({
      query,
      variables
    });
  }

  getLifts(query, variables) {
    return this.apollo.watchQuery({
      query,
      variables
    }).valueChanges;
  }

  getLift(query, variables) {
    return this.apollo.watchQuery({
      query,
      variables
    });
  }

  updateStatus(mutation, variables, refetchQueries?, query?) {
    return this.apollo.mutate({
      mutation,
      variables,
      refetchQueries
    });
  }

  getMakes(query, variables) {
    return this.apollo.use('second').watchQuery({
      query,
      variables
    });
  }

  stripIgnoredCharactersGql(literals: TemplateStringsArray, ...placeholders: any[]) {
    const strippedLiterals = literals.map(str => stripIgnoredCharacters(str));
    return gql(strippedLiterals, ...placeholders);
  }


  subscribeToDataStatusChanges(query, variables, document, updateQuery?) {
    this.apollo.watchQuery({query, variables}).subscribeToMore({
      document,
      variables,
      updateQuery
    });
  }
}
