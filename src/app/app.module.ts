import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

import { GraphQLModule } from './graphql.module';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { LiftsListComponent } from './pages/lifts-list/lifts-list.component';
import { EditLiftStatusComponent } from './pages/edit-lift-status/edit-lift-status.component';
import { MakesListComponent } from './pages/makes-list/makes-list.component';
import { TrailsListComponent } from './pages/trails-list/trails-list.component';

@NgModule({
  declarations: [
    AppComponent,
    LiftsListComponent,
    EditLiftStatusComponent,
    MakesListComponent,
    TrailsListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    GraphQLModule,
    HttpClientModule,
    ReactiveFormsModule,
    SharedModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
