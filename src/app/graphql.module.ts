import {NgModule} from '@angular/core';
import { HttpClientModule, } from '@angular/common/http';

import { APOLLO_OPTIONS, APOLLO_NAMED_OPTIONS  } from 'apollo-angular';

import { ApolloClientOptions, NormalizedCacheObject, InMemoryCache, ApolloLink, selectHttpOptionsAndBody } from '@apollo/client/core';
import { createPersistedQueryLink } from 'apollo-angular-link-persisted';
import { WebSocketLink } from '@apollo/client/link/ws';
import { DefinitionNode, stripIgnoredCharacters } from 'graphql';
import gql from 'graphql-tag';
import { print } from 'graphql/language/printer';
import { HttpLink } from 'apollo-angular/http';

const uri = 'https://snowtooth.moonhighway.com/graphql'; // <-- add the URL of the GraphQL server here
const wss = 'wss://snowtooth.moonhighway.com/graphql'; // <-- add the URL of the GraphQL server here
const uri2 = 'https://olimpo-api.dev.mx.kavak.services/backend-version/version'; // <-- add the URL of the GraphQL server here


const loggerLink = new ApolloLink((operation, forward) => {
  console.log(`GraphQL Request: ${operation.operationName}`);
  operation.setContext({ start: new Date() });
  return forward(operation).map((response) => {
    const responseTime = new Date().getTime() - operation.getContext().start;
    console.log(`GraphQL Response took: ${responseTime}`);
    return response;
  });
});

export function createDefaultApollo(httpLink: HttpLink): ApolloClientOptions<any> {
  const persisted = createPersistedQueryLink({ useGETForHashedQueries: true });
  const http = httpLink.create({ uri });
  const wsClient = new WebSocketLink({
    uri: wss,
    options: {
      reconnect: true,
    },
  });
  const definitionIsSubscription = (d: DefinitionNode) => {
    console.log('What is flowing here', d.kind );
    return d.kind === 'OperationDefinition' && d.operation === 'subscription';
  };

  const link = ApolloLink.split(
    ({ query }) => {
      return query.definitions.some(definitionIsSubscription);
    },
    wsClient,
    persisted.concat(http as any) as unknown as ApolloLink
  );
  return {
      link,
      cache: new InMemoryCache(),
      connectToDevTools: true
  };
}

export function createNamedApollo(httpLink: HttpLink): Record<string, ApolloClientOptions<any>>  {
  const http = httpLink.create({ uri: uri2 });
  const persisted = createPersistedQueryLink({ useGETForHashedQueries: true });
  return {
    second: {
      name: 'second',
      link: loggerLink.concat(persisted.concat(http as any) as unknown as ApolloLink),
      cache: new InMemoryCache()
    }
  };
}

@NgModule({
  exports: [
    HttpClientModule,
    // HttpLinkModule
  ],
  providers: [
    {
    provide: APOLLO_OPTIONS,
    useFactory: createDefaultApollo,
    deps: [HttpLink]
  },
  {
    provide: APOLLO_NAMED_OPTIONS,
    useFactory: createNamedApollo,
    deps: [HttpLink]
  }
]
})
export class GraphQLModule {}


// the pull request wasn't merge
// const minifiedLink = new ApolloLink((operation, forward) => {
//   operation.setContext({ http: {
//     minifyQuery: true,
//     useGETForQueries: true
//   } });
//   return forward(operation).map((response) => {
//     console.log(`GraphQL Response From minified Link: ${response.context}`);
//     return response;
//   });
// });

  // using the ability to split links, you can send data to each link
  // depending on what kind of operation is being sent
  // const definitionIsSubscription = (d: DefinitionNode) => {
  //   return d.kind === 'OperationDefinition' && d.operation === 'subscription';
  // };
  // const link = split(
  //   // split based on operation type
  //   ({ query }) => {
  //     const { kind } = getMainDefinition(query);
  //     return query.definitions.some(definitionIsSubscription);
  //   },
  //   ws,
  //   http,
  // );