import { Component, OnInit } from '@angular/core';
import { GraphQLService } from 'src/app/services/graph-ql.service';
import { gql, QueryRef, Apollo } from 'apollo-angular';

enum TrailStatus {
  OPEN = 'OPEN',
  CLOSED = 'CLOSED',
}

const query = gql`
  query GetTrails($status: TrailStatus) {
    allTrails(status: $status) {
      id
      name
      status
      difficulty
      }
    }
`;

const subscription = gql`
  subscription {
    trailStatusChange {
      id
      name
      status
      difficulty
    }
  }
`;



@Component({
  selector: 'app-trails-list',
  templateUrl: './trails-list.component.html',
  styles: []
})
export class TrailsListComponent implements OnInit {
  closed: QueryRef<any>;
  opened: QueryRef<any>;
  all: QueryRef<any>;
  closedTrails = [];
  openTrails = [];
  allTrails = [];
  loading: any;
  error: any;

  constructor(private gqls: GraphQLService, private apollo: Apollo) { }

  ngOnInit(): void {
    const updateQuery = (prev, {subscriptionData}) => {
      if (!subscriptionData.data) {
        return prev;
      }
      const newFeedItem = subscriptionData.data;
      const idx = prev.allTrails.findIndex(trail => trail.id === newFeedItem.trailStatusChange.id);
      const trail = subscriptionData.data.trailStatusChange;
      const allTrails = prev.allTrails.slice();
      allTrails[idx] = trail;
      console.log('Dentro de la subscripcion', allTrails);
      this.allTrails = allTrails;
      this.openTrails = [];
      this.closedTrails = [];
      this.allTrails.map(trail => {
        if (trail.status === TrailStatus.OPEN) { this.openTrails.push(trail); } else { this.closedTrails.push(trail); }
      });
      return {
        ...prev,
        allTrails
      }
    };

    this.all = this.gqls.getData( query , {  });
    this.all.subscribeToMore({
      document: subscription,
      updateQuery
    });
    this.all.valueChanges.subscribe((result: any) => {
      this.loading = result.loading;
      this.error = result.error;
      this.allTrails = result.data && result.data.allTrails;
      this.allTrails.map(trail => {
        if (trail.status === TrailStatus.OPEN) { this.openTrails.push(trail); } else { this.closedTrails.push(trail); }
      });
    });
  }
}