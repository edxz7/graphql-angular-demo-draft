import { Component, OnInit } from '@angular/core';
import { GraphQLService } from 'src/app/services/graph-ql.service';

enum LiftStatus {
  OPEN = 'OPEN',
  CLOSED = 'CLOSED',
  HOLD = 'HOLD'
}




@Component({
  selector: 'app-lifts-list',
  templateUrl: './lifts-list.component.html',
  styles: []
})
export class LiftsListComponent implements OnInit {
  closedLifts = [];
  openLifts = [];
  holdLifts = [];
  loading: any;
  error: any;

  constructor(private gqls: GraphQLService) { }

  ngOnInit(): void {
    const query = this.gqls.stripIgnoredCharactersGql`
    query GetLifts($status: LiftStatus!) {
      allLifts(status: $status) {
        id
        name
        status
        capacity
      }
    }`;
    console.log('The query was minified?', query.loc.source.body);

    this.gqls.getLifts( query , { status: LiftStatus.CLOSED }).subscribe((result: any) => {
      this.closedLifts = result.data && result.data.allLifts;
      this.loading = result.loading;
      this.error = result.error;
    });
    this.gqls.getLifts( query, { status: LiftStatus.OPEN } ).subscribe((result: any) => {
      this.openLifts = result.data && result.data.allLifts;
      this.loading = result.loading;
      this.error = result.error;
    });
    this.gqls.getLifts( query , { status: LiftStatus.HOLD }).subscribe((result: any) => {
      this.holdLifts = result.data && result.data.allLifts;
      this.loading = result.loading;
      this.error = result.error;
    });
  }
}
