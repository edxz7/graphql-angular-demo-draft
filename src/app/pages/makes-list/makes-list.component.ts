import { Component, OnInit, Query } from '@angular/core';
import { GraphQLService } from 'src/app/services/graph-ql.service';
import { gql } from 'apollo-angular';

const query = gql`
  query Makes($regionId: ID!) {
      makes(regionId: $regionId) {
        name
      }
  }
`;

@Component({
  selector: 'app-makes-list',
  templateUrl: './makes-list.component.html',
  styles: []
})
export class MakesListComponent implements OnInit {
  makes: { name: string }[];
  constructor(private gqls: GraphQLService) { 
    this.gqls.getMakes(query, { regionId: 1 }).valueChanges.subscribe(({data}: any) => {
      this.makes = data;
      console.log(this.makes);
    })
  }

  ngOnInit(): void {
  }

}
