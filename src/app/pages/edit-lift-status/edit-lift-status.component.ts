import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { gql, Apollo } from 'apollo-angular';
import { GraphQLService } from 'src/app/services/graph-ql.service';

enum LiftStatus {
  OPEN = 'OPEN',
  CLOSED = 'CLOSED',
  HOLD = 'HOLD'
}

enum ID {
  'astra-express' = 'astra-express',
  'jolly-roger' = 'jolly-roger',
  'neptune-rope' = 'neptune-rope',
  'panorama' = 'panorama',
  'prickly-peak' = 'prickly-peak',
  'snowtooth-express' = 'snowtooth-express',
  'jazz-cat' = 'jazz-cat',
  'summit' = 'summit',
  'western-states' = 'western-states',
  'wallys' = 'wallys',
  'whirlybird' = 'whirlybird'
}

const GET_LIFT = gql`
  query GetLift($id: ID!) {
    Lift(id: $id){
      id
      status
    }
}
`;

@Component({
  selector: 'app-edit-lift-status',
  templateUrl: './edit-lift-status.component.html',
  styles: []
})
export class EditLiftStatusComponent implements OnInit {
  liftStatusForm: FormGroup;
  keys = Object.keys;
  liftId = '';
  status = LiftStatus;
  idNames = ID;
  constructor(private fb: FormBuilder, private gqls: GraphQLService, private apollo: Apollo) { }

  ngOnInit(): void {
    this.liftStatusForm = this.fb.group({
      id: [null , [Validators.required ], ],
      status: [null, Validators.required]
    });
  }

  get statusInput() {
    return this.liftStatusForm.get('status');
  }

  setLiftId(event) {
    this.liftId = event.target.value;
  }

  handleForm( ) {
    // Mutations
    const UPDATE_STATUS = gql`
      mutation SetLiftStatus($id: ID!, $status: LiftStatus!) {
        setLiftStatus(id: $id, status: $status) {
          id
          status
        }
      }
    `;
    // query to update changes. (we do refetch to reorder the columns, the data is updated in the UI but 
    // it's not in the right place, with refetch, it reorders correctly)
    const query = gql`
      query updateCache($status: LiftStatus!) {
        allLifts(status: $status) {
          id
          status
        }
      }
    `;

    // This query is made to the server to know the status of the selected lift before we update it
    // in that way we can refetch the data in the rigth places
    this.gqls.getLift(GET_LIFT, { id: this.liftStatusForm.get('id').value }).valueChanges.subscribe((redult: any) => {
      // This code retrieve the data from the store but in this examble here is the first time we call GET_LIFT
      // so until this point it exists in the cache (store);
      // const { Lift } = this.apollo.client.readQuery({
      //   query: GET_LIFT,
      //   variables: {
      //     id: this.liftStatusForm.get('id').value,
      //   }
      // });
      // console.log('This data comes from the Store', Lift);

      // the actual status is read from the server
      const status = redult.data && redult.data.Lift.status;
      const refetchQueries = [
        {
          query,
          variables: { status },
        },
        {
          query,
          variables: { status: this.liftStatusForm.get('status').value },
        }
      ];
      this.gqls.updateStatus(UPDATE_STATUS, this.liftStatusForm.value, refetchQueries)
      .subscribe(({ data }) => console.log('Response from the mutation', data));
    });
  }
}
