import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LiftsListComponent } from './pages/lifts-list/lifts-list.component';
import { EditLiftStatusComponent } from './pages/edit-lift-status/edit-lift-status.component';
import { MakesListComponent } from './pages/makes-list/makes-list.component';
import { TrailsListComponent } from './pages/trails-list/trails-list.component';


const routes: Routes = [
  { path: '', component: LiftsListComponent },
  { path: 'trails', component: TrailsListComponent },
  { path: 'makes', component: MakesListComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
